import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiProperty } from '@nestjs/swagger';
import { PuppeteerService } from 'src/puppeteer.service';
import { GoogleMapService } from 'src/google-map.service';

class ScrapingResult {
  @ApiProperty()
  origin: string;
  @ApiProperty()
  destination: string;
  @ApiProperty()
  distance: string;
  @ApiProperty()
  duration: string;
}

@ApiTags('scraping')
@Controller()
export class ScrapingController {

  constructor(private readonly puppeteerService: PuppeteerService, private readonly googleMapService: GoogleMapService) {}
  
  @ApiResponse({status: 200, type: ScrapingResult})
  @Get('scrapingUrl')
  public async scrapingUrl(@Query('url') url: string): Promise<ScrapingResult> {
    const scraping = await this.puppeteerService.scrapingUrlTwoCities(url);
    const distance = await this.googleMapService.evalDistance(scraping.origin, scraping.destination);
    return {...scraping, ...distance};
  }
}
