import { GoogleMapService } from './google-map.service';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PuppeteerService } from './puppeteer.service';
import { ScrapingController } from './scraping/scraping.controller';

@Module({
  imports: [PuppeteerService],
  controllers: [ScrapingController, ScrapingController, AppController],
  providers: [GoogleMapService, PuppeteerService, AppService],
})
export class AppModule {}
