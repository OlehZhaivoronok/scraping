import { Injectable } from '@nestjs/common';
import {launch, Browser} from 'puppeteer';

interface TwoCities {
  origin: string;
  destination: string;
}

@Injectable()
export class PuppeteerService {
  private browser: Browser;
  private async onModuleInit() {
    this.browser = await launch({ 
      headless: true 
    });
  }
  private onModuleDestroy() {
    this.browser.close();
  }

  public async scrapingUrlTwoCities(
    url: string,
    incorrectExec = false,
  ): Promise<TwoCities> {
    if (!incorrectExec) {
      const page = await this.browser.newPage();
      try {
        await page.goto(url);
        await page.waitForSelector('input.tactile-searchbox-input');
        const pageData = await page.evaluate(() => {
          const inputNodeList = document.querySelectorAll(
            `input.tactile-searchbox-input`,
          ) as any;
          const titleNameArray = [];
          for (let i = 0; i < inputNodeList.length; i++) {
            titleNameArray[i] = {
              title: inputNodeList[i].innerText.trim(),
              link: inputNodeList[i].getAttribute('href'),
              val: inputNodeList[i].value,
            };
          }
          return titleNameArray;
        });
        if (pageData && pageData.length !== 2) throw new Error('Incorrect url parsing')
        return { origin: pageData[0].val, destination: pageData[1].val };
      } finally {
        page.close();
      }
    } else {
      try {
        const page = await this.browser.newPage();
        await page.goto(url);
        await page.waitForSelector('input.tactile-searchbox-input');
        // https://www.google.com.ua/maps/dir/fresno/san-francisco
        const pageData = await page.evaluate(() => {
          const citiesSelector = document.querySelectorAll(
            'input.tactile-searchbox-input',
          );
          return citiesSelector;
        });
        return { origin: url, destination: '' + pageData.length };
      } finally {
        await this.browser.close();
      }
    }
  }
}
