import { Client } from '@googlemaps/google-maps-services-js';
import { Injectable } from '@nestjs/common';

const apiKey = 'AIzaSyAYLUJxWQmhQfb9n5vnMD5M4Yh0d5YgIGo';
@Injectable()
export class GoogleMapService {
  public async evalDistance(
    origin: string,
    destination: string,
  ): Promise<{ distance: string; duration: string }> {
    const client = new Client({});
    const resp = await client.distancematrix({
      params: { origins: [origin], destinations: [destination], key: apiKey },
    });
    const distance = resp?.data?.rows[0]?.elements[0]?.distance?.text || '';
    const duration = resp?.data?.rows[0]?.elements[0]?.duration?.text || '';
    return { distance, duration };
  }
}
